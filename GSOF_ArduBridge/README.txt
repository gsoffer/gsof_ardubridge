==========
 GSOF-Arduino Bridge Protocol stack (GSOF_ArduBridge)
==========

Author
======
Guy Soffer
gsoffer@yahoo.com
Israel and LA-California

Overview
========
The code package was designed and written by me (Guy Soffer) as part of my Thesis in Synthetic-Biology and the usage of Digital Microfluidic devices
(Under the supervision of Dr. Steve Shih at Concordia university).
I developed the software to be used by the students in the lab (Many thanks for James Perry for helping me testing it, giving me good ideas and quality coffee breaks).
As far as I can tell, it works well for me and everyone else in the lab for at least three years. It also appers in two publications:
- Moazami, E., Perry, J.M., Soffer, G., Husser, M.C. and Shih, S.C., 2019. Integration of World-to-Chip Interfaces with Digital Microfluidics for Bacterial Transformation and Enzymatic Assays. Analytical chemistry, 91(8),pp.5159-5168.
- Leclerc, L.M., Soffer, G., Kwan, D.H. and Shih, S.C. 2019. A fucosyltransferase inhibition assay using image-analysis and digital microfluidics. Biomicrofluidics 13(3), p.034106.


Documentation
=============
Comment inside the code
Few UDP style block diagrams

Examples
========
None

Installation
============
python setup.py install
or just run setup.bat
